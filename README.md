# BackUpScale Customer Tools

This software repository contains documentation and tools for [BackUpScale](https://backupscale.com/), the automated client-side encrypted off-site backup service with ransomware protection.

Please see [the documentation site](https://docs.backupscale.com/) for more information.
