Customer Tools README.md

We need a place for customers to stick the SSH key

Generate a keygen pair on the VM for the root user of the VM you are backing up

One user 

`sudo ssh-keygen -t ed25519 -C "Server Key for XXX"` <- Human readable key identifier [optional]

`mkdir --parents ~/.ansible/collections`

Clone the repo at https://gitlab.com/backupscale/customer-tools.git into the `~/.ansible/collections` directory


`ansible-playbook --inventory localhost, --ask-become-pass --connection=local ~/.ansible/collections/customer-tools/playbooks/configure-backup-sources.yml`




#TODO Check if it's possible to mask the passphrase when it is entered so that they can tell how many characters are entered

#TODO Add the backupcale configuration files (etc.) to the default [backup] in 90.restic

You can exclude particular files and/or directories using the `exclude` lines

#TODO modify README to include --ask-become-pass (as above)

