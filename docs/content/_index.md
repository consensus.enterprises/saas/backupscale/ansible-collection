---
title: "BackUpScale Documentation"
date: 2024-12-11T12:47:10-04:00
description: "Documentation for BackUpScale, the automated client-side encrypted off-site backup service with ransomware protection"

---

## About this site

This is the documentation site for [BackUpScale](https://backupscale.com/), the automated client-side encrypted off-site backup service with ransomware protection.

It was developed using the principles of the [Diataxis Framework](https://diataxis.fr/), to maximize its usefulness for people at all stages of use.

## Sections

- I am just getting started and want to start using it... [Getting Started](how-tos/getting-started)
- I already have BackUpScale installed and I want to... [Ongoing Operations](how-tos/ongoing-operations)
- I want to know more about the details of how you have implemented this solution, and understand how it works.
    * [Reference](reference)
    * [Explanations](explanations)

There is also [a technical backgrounder available on backupscale.com](https://backupscale.com/technical_details/).

## Change requests

Change requests are welcome.

If you have any suggestions for improvements, please either (in order of our preference):
1. Click on the Edit (pencil) icon on the top-right of any page to submit [a merge request (MR)](https://en.wikipedia.org/wiki/Distributed_version_control#Pull_requests) with your suggested changes.
2. Search for the same issue in [our issues queue](https://gitlab.com/backupscale/customer-tools/-/issues), and hit the 👍 button on it if it's the same one after reviewing it.  If you don't see it, [create a new issue](https://gitlab.com/backupscale/customer-tools/-/issues/new).
3. Reach out to us using one of the methods on [our community page](https://backupscale.com/community/).
