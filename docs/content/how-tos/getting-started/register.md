---
title: "Registration"
date: 2024-12-11T12:47:10-04:00
weight: 1

---

In order to use the service, you'll need a subscription to it, either via the hosted SaaS version at [backupscale.com](https://backupscale.com/) or an account set up with your organization for the corporate installation (self-hosted).

* For the SaaS version, [sign up on the BackUpScale dashboard site](https://dashboard.backupscale.com/signup), and then select and purchase a subscription plan.
* For the Enterprise (self-hosted) version, talk to the folks managing it for your organization.

In either case, you'll need:

* Your username obtained through registration.
* One or more *backup sources*.  These are the devices that that you'd like backed up.

<!---
### SaaS version via Backupscale.com

**During our Beta round, registration is being handled manually by BackUpScale staff.**

To obtain a username, please use [our contact form](https://dashboard.backupscale.com/contact/general_inquiry), and include:

- Contact information (full legal name, e-mail address, and phone number)
- Your preferred username
- One or more [public keys](/tutorials/configure) used for authenticating your backup sources
-->
