---
title: "Backup Source Requirements"
date: 2024-12-11T12:47:10-04:00
weight: 2

---

You can have as many backup sources as you'd like with no additional cost.  For each one you'd like to back up, ensure you are prepared to configure them with the steps below.

## Generate an SSH key pair

Each backup source must have a [Secure Shell (SSH)](https://en.wikipedia.org/wiki/Secure_Shell) [key pair](https://en.wikipedia.org/wiki/Public-key_cryptography) owned by the Administrator or [superuser `root`](https://en.wikipedia.org/wiki/Superuser) used for connecting to the backup service.  The public keys must be provided to the service before you start running backups, ideally as part of registration.  You can use the same key pair for all of your backup sources, different key pairs for each, or some other combination.  As long as each device can connect the the backup serivce over SSH using one of your private keys, you're good to go.

For the purposes of this exercise, we're assuming that you don't already have a key pair to use. If you do, you can skip these steps.

1. Ensure that you have Administrator/`root` access on your device.
2. Start a terminal session as that user (or if you can, prefix the following command with `sudo` as it's safer).
1. Generate a key pair:
    * `ssh-keygen -t ed25519 -C "device1"`
1. When asked where to save the key, the default location should be fine.
1. When asked for a passphrase, **DO NOT** enter one; leave it blank.  Password-protected keys will not work with the automation.
1. Keep the generated public key, typically `id_ed25519.pub`, handy as you'll need to provide it when setting up your account.

You can now either copy this key pair to the same location on your other devices, or generate other key pairs for them the same way.

## Generate and Store a Passphrase
1. Your backups are encrypted using a passphrase which you will select and enter during the configuration stage.
1. **This passphrase cannot be recovered by the system if you lose it! It is required for restoring your backups. You must store it in a safe place!**
> [!TIP]
> If you have an organizational or personal vault that's encrypted (if digital) or secure physically (if not, like an actual safe), that would be the best place to store this.
>
> As most folks aren't going to bother with a physical safe, either one they control or a safety deposit box at the bank, the next best option is a digital vault (which will also generate passwords for you), ideally controlled by you.  A good example of this is [KeePassXC](https://keepassxc.org/).  Otherwise, you can use one of several encrypted password management services, for example:
> * [Proton Pass](https://proton.me/pass)
> * [Bitwarden](https://bitwarden.com/)
>
> We're not endorsing these, but are providing them as options you can look into yourself.  You're on your own if tney cause any problems for you.

> [!CAUTION]
> 1. Don't forget or lose your credentials or access to your vault.
> 2. Keep a backup of your secrets just in case something happens to the physical location or digital service.

> [!INFO]
> Yes, we take backups very seriously.
