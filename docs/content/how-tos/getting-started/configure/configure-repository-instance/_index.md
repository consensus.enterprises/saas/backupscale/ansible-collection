---
title: "Repository Instance Configuration"
date: 2024-12-11T12:47:10-04:00
weight: 2

---

On the left sidebar, click on Repositories -> Add Repo.
![Repositories: Add Repo](add-repository.png)

Then, fill out the form.  There's an image of a filled form following the steps below.

### Form Steps

1. **Repo Name:** Enter whatever you'd like here; this is for identifying your repository in this user interface.
2. **Repository URI:** Enter the following, with your username for *USER_NAME*, and the ID you'd like for your repository for *REPOSITORY_ID* (but note the Caution below).
    * `rclone:location1:backupscale/USER_NAME/REPOSITORY_ID`
> [!CAUTION]
> Repository IDs must be between 3 and 63 characters (inclusive).  They can contain lowercase letters, numbers and dashes (but not at the beginning or end).
3. **Password:** Leave this field blank.  We don't need to generate one here because we'll be using the passphrase we generated during the initial set-up.
4. **Env Vars:** Enter the following to set up your environment with your passphrase.
    * `RESTIC_PASSWORD_FILE=/etc/restic/password`
5. **Flags:** Here are the settings for working with BackUpScale repositories.  Again, replace *User_NAME* with your username.
    * `--option rclone.program="ssh USER_NAME@backups.backupscale.com rclone"`
    * `--option rclone.args="serve restic --stdio --b2-hard-delete --drive-use-trash=false --verbose"`
    * `--verbose`
> [!INFO]
> [Rclone](https://rclone.org/) is the tool we use to marshal your encrypted files to and from our cloud storage service.
6. **Prune Policy:** Hit the *Disabled* button.  This option is for removing no-longer-needed data from your repository, but we can't automate this here because your repositories are normally in append-only mode to enhance security.  Therefore, data cannot be removed with this method.  To do so, see [Removing Snapshots](../../../ongoing-operations/removing-snapshots) for information on how to do it elsewhere.
7. **Check Policy:** This isn't required, but if you'd like to verify the integrity of your repository periodically, you can enable this by setting it to something higher than 0%.  However, this this is a resource-intensive operation so we don't recommend checking more than 10% at once.
8. **Command Modifiers:** Feel free to set these to something other than the defaults to match your resource management requirements.
9. **Auto Unlock**:  Leave this disabled.
> [!CAUTION]
> Enabling this could corrupt your backup repository.  Leave it off unless you really know what you're doing.
10. **Hooks:** You can hook into operations run here.  See [Hook Details](https://garethgeorge.github.io/backrest/docs/hooks) for more information.
11. **Preview:** This is simply for reviewing how the the above configuration will be stored, which can be helpful for automation.
12. Hit the **Submit** button to save the form.

### Example of a filled form

![Exmaple repository configuration](repository-configuration.png)
