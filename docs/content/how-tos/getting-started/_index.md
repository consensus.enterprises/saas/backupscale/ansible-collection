---
title: "Getting Started"
date: 2024-12-11T12:47:10-04:00
weight: 1

---

## Getting Started

1. [Register](register) for an account to use the service.
2. [Prepare](prepare) for setting up the devices you'd like backed up, your *backup sources*.
3. [Install](installation) the software needed to run on your backup sources.
4. [Configure](configure) backup sources.
