---
title: "Backup Source Installation"
date: 2024-12-11T12:47:10-04:00
weight: 3

---

## Add your devices to the backup network

### Requirements

* **Confirmation** that your account has been set up
* The **administrator password** for your logged in user (needed for running the playbook)
* Your **generated passphrase** you've prepared

### Installation

Determine how you'd like to install the necessary software on your devices.  All of the software you need to install has publicly available code (open source) so it can be audited if necessary.  For details, see [Installation Software](../../reference/installation_software.md).

We provide an [Ansible](https://en.wikipedia.org/wiki/Ansible_(software)) [role](https://docs.ansible.com/ansible/latest/user_guide/playbooks_reuse_roles.html) and default [playbook](https://docs.ansible.com/ansible/latest/user_guide/playbooks_intro.html) for automatic configuration of devices, with [this repository](https://gitlab.com/backupscale/customer-tools) actually structured as an [Ansible collection](https://docs.ansible.com/ansible/latest/user_guide/collections_using.html) containing both. If your organization uses another automated provisioning tool enabling [infrastructure as code (IaC)](https://en.wikipedia.org/wiki/Infrastructure_as_code) such as [Salt](https://en.wikipedia.org/wiki/Salt_(software)), [Chef](https://en.wikipedia.org/wiki/Chef_(software)) or [Puppet](https://en.wikipedia.org/wiki/Salt_(software)), feel free to write an equivalent script (and then provide it to us so that we can share it with others).  If you prefer to configure your backup sources manually (not recommended, but necessary if you have different set-up requirements, such as [running the Backrest console in a Docker container instead of locally](https://hub.docker.com/r/garethgeorge/backrest)), you can review [the included role](https://gitlab.com/backupscale/ansible-collection/-/tree/master/roles/backup_source) to follow and possibly modify those steps.

#### Using our Ansible role

For more information on what our Ansible role actually does on your device, please review [the role overview](https://gitlab.com/backupscale/customer-tools/-/tree/master/roles/backup_source?ref_type=heads#overview).

##### For a single backup source, run locally on the backup source or remotely targetting it

2. Install Ansible by running the following command:
    * `sudo apt install ansible`
3. Add a directory for Ansible collections:
    * `mkdir --parents ~/.ansible/collections`
5. Clone this repository into your collections directory:
    * `git clone https://gitlab.com/backupscale/customer-tools.git ~/.ansible/collections/customer-tools`
6. Run the Ansible playbook.
    * Locally: `ansible-playbook --inventory localhost, --ask-become-pass --connection=local ~/.ansible/collections/customer-tools/playbooks/configure-backup-sources.yml`
    * Remotely: `ansible-playbook --inventory REMOTE_HOSTNAME, --ask-become-pass ~/.ansible/collections/customer-tools/playbooks/configure-backup-sources.yml`
7. When it prompts you for a ***BECOME password***, enter your own password to get `sudo` access so that some tasks can run with administrator permissions (only the ones that need them).

##### Multiple remote targets, non-local

For multiple targets that aren't local, you'd instead run something like this, or provide [an inventory file](https://docs.ansible.com/ansible/latest/inventory_guide/intro_inventory.html):
1. `ansible-playbook --inventory server1,server2,server3 /path/to/this/repository/playbooks/configure-backup-sources.yml`
1. When it prompts you for a ***BECOME password***, enter your own password to get `sudo` access so that some tasks can run with administrator permissions (only the ones that need them).

### Next Steps

We're now ready to move onto [configuration](configure).
