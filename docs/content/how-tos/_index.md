---
title: "How To Guides"
date: 2024-12-11T12:47:10-04:00
weight: 1

---

<!---
This is the front page for the How-To Guides section.

These are more generalized instructions when somebody says (for example) "How do I add a new server to my backups?"
-->

### Sections

* [Getting Started](getting-started)
* [Ongoing Operations](ongoing-operations)
