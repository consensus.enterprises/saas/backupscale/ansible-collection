---
title: "Removing Snapshots"
date: 2024-12-11T12:47:10-04:00
weight: 1

---

## Background

Over time, backup repositories can become large.  Because backup space isn't free, and unnecessarily large repositories can become unwieldy, it's important to remove snapshots to prevent this from happening.

From [the Restic documentation](https://restic.readthedocs.io/en/stable/060_forget.html):
> This can be done either manually (by specifying a snapshot ID to remove) or by using a policy that describes which snapshots to forget. For all remove operations, two commands need to be called in sequence: `forget` to remove snapshots, and `prune` to remove the remaining data that was referenced only by the removed snapshots. The latter can be automated with the `--prune` option of `forget`, which runs `prune` automatically if any snapshots were actually removed.

> [!NOTE]
> Pruning snapshots can be a time-consuming process, depending on the number of snapshots and data to process. During a prune operation, the repository is locked and backups cannot be completed. Please plan your pruning so that there’s time to complete it and it doesn’t interfere with regular backup runs.

## Constraints

Because BackUpScale prioritizes security, repositories generally operate in append-only mode (backup snapshots can be written, but repository data cannot be deleted), these operations cannot typically be run on their own.

The high-level process is thus:
1. Disable append-only mode.
2. Remove snapshots.
3. Re-enable append-only mode.

> [!WARNING]
> Do not forget to re-enable append-only mode.  Doing so will make your backup repositories vulnerable to ransomware attacks: If any of your devices become compromised, attackers can delete all of your backups from those devices. Keep these maintenance windows as short as possible.

> [!INFO]
> While in the early discussion stages, we're planning to add a per-key overrides feature that will allow specific keys to force append-only mode or not.  This will allow you to maintain a device in a more secure environment whose key will allow it to automatically run these maintenance operations without manual intervention.  While this strategy still has its risks, it provides another option.

## Process

### Disable append-only mode

1. Log in to [the dashboard site](https://dashboard.backupscale.com/).
2. Navigate to your account settings.
3. Check the *Disable append-only mode?* box.
4. Save your settings.
5. Wait several minutes for your account to be updated.
6. You should receive an e-mail reminding you that append-only mode is disabled.

### Remove snapshots

1. Log into the Backrest console from your device ([http://localhost:9898/](http://localhost:9898/) if local).
2. From the left sidebar, navigate to Repositories -> your repository.
3. Hit the *Run Command* button.
![Run command dialog box](run-command.png)
4. Run a command like the following example to enforce your chosen retention policy (see [the Restic documentation](https://restic.readthedocs.io/en/stable/060_forget.html#removing-snapshots-according-to-a-policy) for details on how to modify for your requirements).  Snapshots that aren't retained will be deleted.
    * `forget --dry-run --keep-within 1m --keep-within-daily 7d --keep-within-weekly 1m --keep-within-monthly 1y --keep-within-yearly 75y --prune`
> [!NOTE]
> This command won't actually delete any data because of the `--dry-run` option.  When you're satisfied with what it would do, you can rerun that command without that option, which will actually mark the data for deletion, and then delete it (because of `--prune`).
5. Hit the *Index Snapshots* button to notify the Backrest console about your changes.

### Re-enable append-only mode

1. Log in to [the dashboard site](https://dashboard.backupscale.com/).
2. Navigate to your account settings.
3. Uncheck the *Disable append-only mode?* box.
4. Save your settings.
5. Wait several minutes for your account to be updated.
6. You can then delete the reminder e-mail from your inbox!
