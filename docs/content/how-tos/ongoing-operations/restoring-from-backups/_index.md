---
title: "Restoring from Backups"
date: 2024-12-11T12:47:10-04:00
weight: 2

---

1. Log into the Backrest console from your device ([http://localhost:9898/](http://localhost:9898/) if local).
2. From the left sidebar, navigate to *Plans -> your plan*.
3. Under the *Tree View* of snapshots, select an appropriate backup to restore from, and click on it.
![Tree view of snapshots](tree-view-of-snapshots.png)
> [!TIP]
> Try to think of the most recent time before your file or folder was corrupted or missing.  If you realize afterwards that you were incorrect, try an earlier one.
4. In the right-side pane, click on *Snapshot Browser*, which will show the file tree.
5. Click on the arrow to the left of the displayed directory/folder to expand it, and then keep doing so down the tree until you find the item you'd like to restore.
![Snapshot browser](snapshot-browser.png)
6. Hover over it, all way to the right, until you're over the little menu icon.
7. You should then see a menu with options *Info* and *Restore to path*.
8. Click on *Restore to path*.  You should then see the following pop-up.
![Pop-up prompting for the path where file should be restored](restore-pop-up.png)
9. Enter the path where you'd like the item restored.  The original location should be the default, you can enter another location, or you can leave the field blank to place it in your Downloads folder.
> [!CAUTION]
> The item being restored will overwrite whatever you have in the chosen location with the same name.  If you'd like to keep that copy, place the item elsewhere.
10. Review the item locally.  If it doesn't contain what you want, try again with another snapshot that will likely contain what you're looking for.
