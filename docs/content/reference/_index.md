---
title: "Reference Materials"
date: 2024-12-11T12:47:10-04:00
weight: 3

---

<!---
From the [Diataxis framework materials](https://diataxis.fr/reference/):

"REFERENCE GUIDES ARE TECHNICAL DESCRIPTIONS OF THE MACHINERY AND HOW TO OPERATE IT. REFERENCE MATERIAL IS INFORMATION-ORIENTED."
-->

1. [Installation software](installation_software.md)
